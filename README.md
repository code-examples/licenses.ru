# [licenses.ru](https://licenses.ru) source codes

<br/>

### Run licenses.ru on localhost

    # vi /etc/systemd/system/licenses.ru.service

Insert code from licenses.ru.service

    # systemctl enable licenses.ru.service
    # systemctl start licenses.ru.service
    # systemctl status licenses.ru.service

http://localhost:4060
